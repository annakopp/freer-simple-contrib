# freer-simple-contrib

This is a collection packages related to working the [`freer-simple`](http://hackage.haskell.org/package/freer-simple) effects package. 


### :clock: [freer-simple-time](./freer-simple-time) [![Hackage](https://img.shields.io/hackage/v/freer-simple-time.svg)](http://hackage.haskell.org/package/freer-simple-time)

* `Time` effect for working with time. 

### :dizzy: [freer-simple-random](./freer-simple-random) [![Hackage](https://img.shields.io/hackage/v/freer-simple-random.svg)](http://hackage.haskell.org/package/freer-simple-random)

* `Random xs` effect for generating random values of types `xs`

### :spider_web: [freer-simple-http](./freer-simple-http) [![Hackage](https://img.shields.io/hackage/v/freer-simple-http.svg)](http://hackage.haskell.org/package/freer-simple-http)

* `Http` effect for making HTTP requests and decoding responses from JSON

### :exclamation: [freer-simple-catching](./freer-simple-catching) [![Hackage](https://img.shields.io/hackage/v/freer-simple-catching.svg)](http://hackage.haskell.org/package/freer-simple-catching)

* `Catching f e` effect for handling exceptions of type `e` that are thrown during the _interpretation_ of effects of type `f`

### :stopwatch: [freer-simple-profiling](./freer-simple-profiling) [![Hackage](https://img.shields.io/hackage/v/freer-simple-profiling.svg)](http://hackage.haskell.org/package/freer-simple-profiling)

* Automatic profiling of effectful computations! 