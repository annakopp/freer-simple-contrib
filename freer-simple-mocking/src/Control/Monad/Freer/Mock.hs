module Control.Monad.Freer.Mock
  ( module Control.Monad.Freer.Mock
  , module Control.Monad.Freer.Mock.HList
  , module Control.Monad.Freer.Mock.Mockable
  , module Control.Monad.Freer.Mock.Types
  ) where

import           Control.Monad.Freer
import           Control.Monad.Freer.Mock.HList
import           Control.Monad.Freer.Mock.Mockable
import           Control.Monad.Freer.Mock.Types
import           Data.Kind
import           Data.Proxy
import           Type.Reflection
import           Unsafe.Coerce

data Mock f where
   (:=>) :: (Typeable f, Typeable a, Show a, Eq (MatchList f xs (k a))) => MatchList f xs (k a) -> a -> Mock k

mock :: forall r v f . (Member f r, Mockable f) => [Mock f] -> Eff r v -> Eff r v
mock choices = interpose (handle choices)
  where
    handle :: forall a . [Mock f] -> f a -> Eff r a
    handle [] action = send action
    handle (check:rest) action = case check of
      (request :: MatchList requestT xs (f q)) :=> result -> case promote action of
        SomeType (Proxy :: Proxy actionT) actionArgs -> case eqTypeRep (typeRep @requestT) (typeRep @actionT) of
          Just HRefl -> if request == (unsafeCoerce $ toMatchList actionArgs)
            then return $ unsafeCoerce result
            else handle rest action
          Nothing -> handle rest action

    toMatchList :: HList xs -> MatchList fa xs r
    toMatchList HNil        = MNil
    toMatchList (HCons x y) = (Exactly x) :+ (toMatchList y)


