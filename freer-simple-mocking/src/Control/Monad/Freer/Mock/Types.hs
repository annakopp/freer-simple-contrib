{-# LANGUAGE AllowAmbiguousTypes #-}

module Control.Monad.Freer.Mock.Types
  ( Matcher(..)
  , Stacked
  , Return
  , Args
  , MatchList(..)
  , given
  , matching
  ) where

data Matcher a = Exactly a | Any

instance Eq a => Eq (Matcher a) where
  Any == _ = True
  _ == Any = True
  Exactly a == Exactly b = a == b

type family MatchArgs xs where
  MatchArgs '[] = '[]
  MatchArgs (x ': xs) = (Matcher x) ': MatchArgs xs

type family Stacked xs where
  Stacked '[] = ()
  Stacked (x ': xs) = (x, Stacked xs)

type family ArgsK f where
  ArgsK (a -> b) = a ': ArgsK b
  ArgsK b = '[]

type family Args f where
  Args (f :: k) = ArgsK k

type family ReturnK f where
  ReturnK (a -> b) = ReturnK b
  ReturnK b = b

type family Return f where
  Return (f :: k) = ReturnK k

data MatchList f xs r where
  MNil :: MatchList f '[] r
  (:+) :: Matcher a -> MatchList f as r -> MatchList f (a ': as) r

instance Eq (MatchList f '[] r) where
  _ == _ = True

instance (Eq x, Eq (MatchList f xs r)) => Eq (MatchList f (x ': xs) r) where
  (x :+ xs) == (y :+ ys) = x == y && xs == ys

class Given r where
  stackedGiven :: r

instance Given (() -> MatchList f '[] r) where
  stackedGiven _ = MNil

instance Given (a -> MatchList f xs r) => Given ((x, a) -> MatchList f (x ': xs) r) where
  stackedGiven (x, a) = (Exactly x :+) $ stackedGiven @(a -> MatchList f xs r) a

given :: forall a
       . ( Curry (Stacked (Args a))
         , Given ((Stacked (Args a)) -> MatchList a (Args a) (Return a))
         )
      => AsFun (Stacked (Args a)) (MatchList a (Args a) (Return a))
given = curryAll @(Stacked (Args a)) @(MatchList a (Args a) (Return a)) stackedGiven

class Matching r where
  stackedMatching :: r

instance Matching (() -> MatchList f '[] r) where
  stackedMatching _ = MNil

instance Matching (a -> MatchList f xs r) => Matching ((Matcher x, a) -> MatchList f (x ': xs) r) where
  stackedMatching (x, a) = (x :+) $ stackedMatching @(a -> MatchList f xs r) a

matching :: forall a
          . ( Curry (Stacked (MatchArgs (Args a)))
            , Matching ((Stacked (MatchArgs (Args a))) -> MatchList a (Args a) (Return a))
            )
         => AsFun (Stacked (MatchArgs (Args a))) (MatchList a (Args a) (Return a))
matching = curryAll @(Stacked (MatchArgs (Args a))) @(MatchList a (Args a) (Return a)) stackedMatching

class Curry x where
  type AsFun x a :: *

  curryAll :: (x -> a) -> AsFun x a

instance Curry () where
  type AsFun () a = a

  curryAll = ($ ())

instance Curry y => Curry (x, y) where
  type AsFun (x, y) a = x -> AsFun y a

  curryAll f x =
    let smaller = curryAll @y
    in smaller $ curry f x
