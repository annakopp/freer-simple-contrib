# :clock: freer-simple-time [![Hackage](https://img.shields.io/hackage/v/freer-simple-time.svg)](http://hackage.haskell.org/package/freer-simple-time)

This library provides an effect `Time` with a single function, `currentTime` which ... gets the current time.

Also included are a few interpreters, some pure, some in `IO`