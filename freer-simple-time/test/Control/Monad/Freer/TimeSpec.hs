module Control.Monad.Freer.TimeSpec where

import Control.Monad.Freer.Time

import Control.Monad.Freer
import Test.Hspec

spec :: Spec
spec = do
  describe "runTimeAt" $ do
    it "should give the expected time" $ do
      let time = read "2018-11-10 18:39:54.228369 UTC"
          gotTime = run $ runTimeAt time currentTime
      gotTime `shouldBe` time
      
  describe "runInstantaneously" $ do
    it "should make all times seem the same" $ do
      let getTwoTimes = (,) <$> currentTime <*> currentTime
      (timeA, timeB) <- runM $ runTime getTwoTimes
      timeA `shouldNotBe` timeB
      (timeC,  timeD) <- runM $ runInstantaneously getTwoTimes
      timeC `shouldBe` timeD

