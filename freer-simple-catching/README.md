# freer-catching

This is library for working with exceptions that are thrown during interpreation of free monad effects, at call time.

## How to use

This library exposes one effect:

```haskell
catching :: forall e f a r
          . (Exception e, Member (Catching f e) r)
         => Eff '[f] a
         -> Eff r (Either e a)
```

We take an effectful function over one effect `f` and we turn it into an effectful computation of a new effect which denotes that we have an obligation to catch exceptions when we interpret this effect.

We can then discharge this obligation by interpretting the effect `f` and and any `Catching` effects built on `f` by interpretting them both at the same time: 

```haskell
runCatching :: forall e r v f
             . (Member IO r, Member IO (f ': r))
            => (forall a q . Member IO q => Eff (f ': q) a -> Eff q a)
            -> Eff (Catching f e : f : r) v
            -> Eff r v
``` 

We can also interpret a `Catching f e` effect by way of another effect `g`:

```haskell
runCatching2 :: forall e r v f g
             . (Member IO r, Member IO (f : r), Member IO (f : g : r))
            => (forall a q. Member g q => Eff (f : q) a -> Eff q a)
            -> (forall a q. Member IO q => Eff (g : q) a -> Eff q a)
            -> Eff (Catching f e : f : g : r) v
            -> Eff r v
```            