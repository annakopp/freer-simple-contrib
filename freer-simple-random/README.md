# :dizzy: freer-simple-random [![Hackage](https://img.shields.io/hackage/v/freer-simple-random.svg)](http://hackage.haskell.org/package/freer-simple-random)

This library provides an effect `Random (xs :: [*])` for generating random numbers of types within `xs`. 

Also included are interpreters to `IO`, pure interpreters that use a seed, and ways to reduce the set of 
random types (from `[Int, Bool]` -> `[Bool]`, and even from `[Bool]` -> `[]` :ghost:)