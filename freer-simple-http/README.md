# :spider_web: freer-simple-http [![Hackage](https://img.shields.io/hackage/v/freer-simple-http.svg)](http://hackage.haskell.org/package/freer-simple-http)

This library provides an `Http` effect with functions for 
* making http requests
* making http requests with JSON responses (and not having to worry about parse errors every time you try and convert a response body)
* making http requests with JSON responses (and worrying about parse errors, because sometimes you have to)

Also included are an `IO` interpreter (using [`http-client`](http://hackage.haskell.org/package/http-client)) and a handful of pure mocks. 