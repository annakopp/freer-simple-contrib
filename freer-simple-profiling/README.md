# :stopwatch: freer-simple-profiling [![Hackage](https://img.shields.io/hackage/v/freer-simple-profiling.svg)](http://hackage.haskell.org/package/freer-simple-profling)

This library doesn't introduce any new effects. It instead provides a way to automatically profile a computation in `Eff`.

Say we have some effect `User`:

```haskell
data User a where
  GetUsers :: User [(UserId, UserDetails)]
  GetUser :: UserId -> User UserDetails
  NewUser :: UserDetails -> User UserId
  UpdateUser :: UserDetails -> UserId -> User ()
```

We can automatically profile an effectful  `computation :: Eff (User ': r) a` and get a count of how many times we are creating, updating, and fetching, as well as how long each operation takes. All we have to do is call `profileEffect @User computation`. No changing of `computation` is necessary.

Works well with multiple effects, including profling effects which are interpeted to lower-level effects:

`runDBEffect . profileEffect @DB . runUserEffect . profileEffect @User $ computation`